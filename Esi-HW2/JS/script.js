var Url = "test.json";

var xmlHttp = new XMLHttpRequest();
xmlHttp.onreadystatechange = ProcessRequest;
xmlHttp.open( "GET", Url, true );
xmlHttp.send( null );

function ProcessRequest()
{
    if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 )
    {
        var response = JSON.parse(xmlHttp.responseText);

        addTable(response);
}
}

function addTable(response, table) {
      var table = document.createElement("table");
        table.classList.add('table');
      document.body.appendChild(table);

    for (var i=0; i<response.length; i++){
          addRow(response[i], table);
          addCell(response[i], table);
}
}

function addRow(response, table){
      var row = table.insertRow();
        row.style.color="green";

    for (var key in response) {
      var cell = row.insertCell();
        cell.innerHTML = key;
            }
    }
function addCell(response, table) {
      var row = table.insertRow();

    for(var key in response) {
        var cell = row.insertCell();
          cell.style.color="orange";

    if(typeof response[key] === 'object' && response[key] !== null){
      var innerTable = document.createElement("table");
          addRow(response[key], innerTable);
          addCell(response[key], innerTable);
          cell.appendChild(innerTable);

    }
    else {
          cell.innerHTML = response[key];
      }
  }
}