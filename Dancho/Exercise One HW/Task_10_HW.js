/*
Task 10 - homework
Write a function charFreq() that takes a string and builds a frequency
listing of the characters contained in it. Represent the frequency
listing as a Javascript object. Try it with something like
charFreq("abbabcbdbabdbdbabababcbcbab").
*/


function charFreq(str){
    var obj = {};
    for(var i = 0; i < str.length; i++){
        obj[str[i]] = ++obj[str[i]] || 1;
    }
    return obj;
}
    console.log(charFreq("abbabcbdbabdbdbabababcbcbab"));