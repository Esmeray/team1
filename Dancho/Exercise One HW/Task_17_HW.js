/*
Task 17 - homework
Write a function that reorders the items in an array into a random order.
The function does not return anything, but rather moves items around the given array.
There are many ways to solve this exercise, but one of the simplest is to repeatedly
call removeRandomItem() from the previous question to obtain all the items in random
order one by one. Some examples (but note that the results are supposed to be random
and should vary between runs):

 ["foo", "bar", "baz"]: ["bar", "foo", "baz"]
  ["a", "b", "c", "d", "e", "f"]: ["d", "f", "e", "c", "b", "a"]
  []: []


randomizeOrder(["foo", "bar", "baz"])
result  ["bar", "foo", "baz"]

randomizeOrder(["a", "b", "c", "d", "e", "f"])
result ["e", "d", "f", "b", "c", "a"]

randomizeOrder([])
return []
*/


function randomizeOrder(arr) {
    if (arr.length === 0) {
        console.log(arr);
    }
    else {
    var randomValue1 = Math.floor(Math.random() * arr.length);
    var randomItem = arr.splice(randomValue1, 1);
    var randomValue2 = Math.floor(Math.random() * arr.length);
    arr.splice(randomValue2, 0, randomItem[0]);
    console.log(arr);
    };
};

randomizeOrder(["foo", "bar", "baz"]);
randomizeOrder(["a", "b", "c", "d", "e", "f"]);
randomizeOrder([]);