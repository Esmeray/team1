/*
Task 15 - homework
Given a string, find the first appearance of the substring 'not' and 'bad'.
If the 'bad' follows the 'not', replace the whole 'not'...'bad' substring
with 'good' and return the result. If you don't find 'not' and 'bad' in the
right sequence (or at all), just return the original sentence. For example:
 'This dinner is not that bad!': 'This dinner is good!'
  'This dinner is bad!': 'This dinner is bad!'


notBad('This dinner is not that bad!')
result This dinner is good!

notBad('This movie is not so bad')
result This movie is good

notBad('This tea is not hot')
result This tea is not hot

notBad('It's bad yet not')
result It's bad yet not
*/

function notBad(str) {
    var notPosition = str.indexOf('not');
    var badPosition = str.indexOf('bad');
    if (notPosition !== undefined && notPosition !== undefined && notPosition < badPosition) {
        str = 'Result: ' + str.slice(0, notPosition) +  'good.';
    }
    else {
        str = 'No change as result: ' + str;
    }
    console.log(str);
}
notBad('This dinner is not that bad!');
notBad('This movie is not so bad');
notBad('This tea is not hot');
notBad("It's bad yet not");