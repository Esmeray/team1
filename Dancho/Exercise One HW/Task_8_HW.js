/*
Task 8 - homework
Write a function findLongestWord() that takes an array of words 
and returns the length of the longest one.
*/

var string = 'This seems to be the easiest way to do this.';
var str = string.split(" ");
var longest = 0;
var word = null;

str.forEach(function(str) {
    if (longest < str.length) {
        longest = str.length;
        word = str;
    }
});
console.log(word);