/*
Task 11 - homework
Given an amount, return '<amount> leva', except add '($$$)' at the end
if the amount is 1 million. For example:

chekValue(10)
result 10 leva

chekValue(42)
result  42 leva

chekValue(1000000)
result 1000000 dollars ($$$)
*/

function checkValue (val) {
    if (val === 1000000) {
        console.log('result ' + val + ' ($$$)')}
    else {
        console.log('result ' + val + ' leva');
    };
};
checkValue(1000000);
checkValue(1000);