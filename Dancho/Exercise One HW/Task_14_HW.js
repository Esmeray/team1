/*
Task 14 - homework
Given a string, if its length is at least 3, add 'ing' to its end.
Unless it already ends in 'ing', in which case add 'ly' instead.
If the string length is less than 3, leave it unchanged.
Return the new string. For example:

verbing('verb')
result verbing

verbing('swimming')
result  swimmingly

verbing('do')
result  do
*/


function add(str) {
    if (str.length < 3) {
        str = str;
    }
    else {
        var ing = "ing";
        if (str.slice(-3) === ing) {
            str = str + 'ly';
        }
        else {
            str = str +ing;
        }
    }
    console.log(str);
}

add('verb');
add('swimming');
add('do');