/*
Task 6 - homework
Define a function reverse() that computes the reversal of a string. 
For example, reverse("jag testar") should return the string "ratset gaj".
*/

var str = 'When a function is stored as a property of an object, we call it a method.';
console.log(str);

str = str.split("").reverse().join("");
console.log('Now it is reversed: \n'  + str);