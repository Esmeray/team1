/*
Task 5 - homework
Define a function sum() and a function multiply() that sums and multiplies
(respectively) all the numbers in an array of numbers. For example,
sum([1,2,3,4]) should return 10, and multiply([1,2,3,4]) should return 24.
*/

function sum (array) {
    var sumValue = 0;
    for (var i = 0; i < array.length; i++) {
        sumValue += array[i];
    };
    console.log(sumValue);
};

function multiply (array) {
    var multiplication = 1;
    for (var i = 0; i < array.length; i++) {
        multiplication *= array[i];
    };
    console.log(multiplication);
};

sum([1,2,3,4]);
multiply([1,2,3,4]);