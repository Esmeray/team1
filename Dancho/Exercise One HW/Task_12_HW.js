/*
Task 12 - homework
Given two strings, return the concatenation of the two strings
(separated by a space) slicing out and swapping
the first 2 characters of each. You can assume that the strings
are at least 2 characters long. For example:

mixUp('mix', 'pod')
result  pox mid

mixUp('dog', 'dinner')
result  dig donner

mixUp('gnash', 'sport')
result  spash gnort
*/


function mixUp(str1, str2) {
    var str1Mixed = str2.slice(0,2) + str1.slice(2);
    var str2Mixed = str1.slice(0,2) + str2.slice(2);
    console.log('result ' + str1Mixed + ' ' + str2Mixed);
};

mixUp('mix', 'pod');
mixUp('dog', 'dinner');
mixUp('gnash', 'sport');