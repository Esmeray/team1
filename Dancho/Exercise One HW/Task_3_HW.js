/*
Task 3 -
Write a function that takes a character (i.e. a string of length 1) and
returns true if it is a vowel, false otherwise.
*/

function check (char) {
    var vowels = ['a','e','i','o','u'];
    for (var i = 0; i < vowels.length; i++) {
        if  (vowels[i] === char)
            return true;
        else
            return false;
    };
};
console.log(check('a'));