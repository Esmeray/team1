/*
Task 16 - homework
Write a function that removes an item from a random location in the given array
and returns the removed item. (Be careful to return just the item, not an array
of length 1 that contains the item.) If the array is empty,
do nothing and return null. Some examples
(but note that the results are supposed to be random and should vary between runs):

  ["foo", "bar", "baz"]: "bar" leaving ["foo", "baz"]
  []: null leaving []

removeRandomItem(["foo", "bar", "baz"])
result  "bar" leaving ["foo", "baz"]

removeRandomItem([42])
result 42 leaving []

removeRandomItem([])
result  null leaving []
*/

function removeRandomItem(array) {
    if (array.length === 0) {
        console.log('null');
    }
    else {
        console.log('Given array: ' + array);
        var randomPosition = Math.floor(Math.random()*array.length);
        var item = array.splice(randomPosition, 1);
        console.log('Random item removed from the array: ' + item);
        console.log('Typeof the Item: ' + typeof(item));
        console.log('And the given array now looks like this: ' + array);
    };
};

removeRandomItem(["foo", "bar", "baz"]);
removeRandomItem([42]);
removeRandomItem([]);
