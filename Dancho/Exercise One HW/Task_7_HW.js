/*
Task 7 - homework
Represent a small bilingual lexicon as a Javascript object in the 
following fashion 
{"merry":"god", "christmas":"jul", "and":"och", "happy":gott", "new":"nytt", "year":"år"} 
and use it to translate your Christmas cards from English into Swedish.
*/

var obj = {"merry":"god", "christmas":"jul", "and":"och", "happy":"gott", "new":"nytt", "year":"år"};
var english = 'merry christmas and happy new year';
english = english.split(" ");
for (var i in english) {
	console.log(english[i] + '-->' + obj[english[i]]);
}