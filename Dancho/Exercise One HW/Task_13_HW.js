/*
Task 13 - homework
Given a string, return a version where all occurrences of its first character
have been replaced with '*', except do not change the first character itself.
You can assume that the string is at least one character long. For example:

fixStart('babble')
result  ba*ble

fixStart('aardvark')
result  a*rdvark

fixStart('google')
result goo*le
*/

var str = 'every consonant and place an occurrence';
str = str.split('');
console.log(str.join(''));

function changeText (str) {
    var newStr = str[0];
    for (var i = 1; i < str.length; i++)
        if (str[0] === str[i])
            newStr += '*';
        else newStr += str[i];
    console.log(newStr);
}
console.log(changeText(str));