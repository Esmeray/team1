/*
Task 2 -
Define a function maxOfThree() that takes three numbers as arguments
and returns the largest of them.
*/


function maxOfThree (a, b, c) {
    var biggest = a;
    if (biggest < b)
        biggest = b;
    else
        if (biggest < c)
            biggest = c;
    console.log(biggest);
};
maxOfThree(11, 2, 90);


// function maxOfThree (a, b, c) {
//     console.log(Math.max(a, b, c));
// }
// maxOfThree(11, 2, 90);