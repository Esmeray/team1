/*Task 17 - homework
Write a function that reorders the items in an array into a random order.
The function does not return anything, but rather moves items around the given array.
There are many ways to solve this exercise, but one of the simplest is to repeatedly call
removeRandomItem() from the previous question to obtain all the items in random order one by
one. Some examples (but note that the results are supposed to be random and should vary between runs):
 ["foo", "bar", "baz"]: ["bar", "foo", "baz"]
  ["a", "b", "c", "d", "e", "f"]: ["d", "f", "e", "c", "b", "a"]
  []: []
randomizeOrder(["foo", "bar", "baz"])
result  ["bar", "foo", "baz"]
randomizeOrder(["a", "b", "c", "d", "e", "f"])
result ["e", "d", "f", "b", "c", "a"]
randomizeOrder([])
return []*/
function reorderArr(arr){
     for (var i = arr.length - 1; i > 0; i--) {
var j = Math.floor(Math.random() * (i+1));
var temp=arr[i];
arr[i]= arr[j];
arr[j]=temp;
}
return arr;
};
console.log(reorderArr(["a", "b", "c", "d", "e", "f"]));
