/*Task 4 - ????
Write a function translate() that will translate a text into "t".
That is, double every consonant and place an occurrence of "o"
in between. For example, translate("this is fun") should return
the string "tothohisos isos fofunon".*/
/*var consonant=new Array("t","s","h","f","n","k");*/
function checkConsonant(string){
    consonant=['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
    var isConsonant=false;

    for(var i=0; i<consonant.length; i++){
        if (string==consonant[i]){
            isConsonant=true;
        }
    }
    return isConsonant;
}

    function translate(nString) {
var newString='';
for(var i=0; i<nString.length;i++){
    if (checkConsonant(nString[i])) {
        newString += nString[i] + '0' + nString[i];
    }
    else {
        newString += nString[i];
    }
}
console.log( 'Translation is: ' + newString + ' ');
}
console.log(translate('this is fun'));
