/*Task 14 - homework
Given a string, if its length is at least 3, add 'ing' 
to its end. Unless it already ends in 'ing', in which case add 'ly' 
instead. If the string length is less than 3, leave it unchanged. Return the new string. For example:
verbing('verb')
result verbing

verbing('swimming')
result  swimmingly

verbing('do')
result  do*/

function checkString(string) {
res='ing';
i=0;
for(i in string)
strPart=string.substr(i-2,i);
strLength=string.length;
if(strLength >2 && strPart !== res) {
return string + res;
}
else if(strLength >2 && strPart === res) {
return string;
}
else if(strLength<=2) { 
return string; }
};
