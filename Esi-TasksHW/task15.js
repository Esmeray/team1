/*Task 15 - homework
Given a string, find the first appearance of the substring 
'not' and 'bad'. If the 'bad' follows the 'not', replace the whole 
'not'...'bad' substring with 'good' and return the result. If you don't find 'not'
 and 'bad' in the right sequence (or at all), just return the original sentence. For example:
 'This dinner is not that bad!': 'This dinner is good!'
  'This dinner is bad!': 'This dinner is bad!'*/
 function checkWords (string) {
 	var stringChange='';
if (string.indexOf('bad')>string.indexOf('not')){
	stringChange = string.substring(0, string.indexOf('not')) + 'good';
	return stringChange;
}
else {
	return string;
}
 };
 console.log(checkWords('This dinner is not that bad!'));
