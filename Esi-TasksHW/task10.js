/*Task 10 - homework
Write a function charFreq() that takes a string
and builds a frequency listing of the characters
contained in it. Represent the frequency listing as a
Javascript object. Try it with something like charFreq("abbabcbdbabdbdbabababcbcbab").*/
function charFreq(string) {
var freq={};
for(var i=0;i<string.length;i++){
    var caracter=string.charAt(i);
    if(freq[caracter]){
    freq[caracter]++;
    }
    else {
        freq[caracter]=1;
    }
}
return freq;
};
console.log(charFreq("abbabcbdbabdbdbabxababcbcbabe"));
